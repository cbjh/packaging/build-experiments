FROM archlinux

RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -Sy base-devel
RUN pacman --noconfirm -Sy git
RUN pacman --noconfirm -Sy python python-pip python-setuptools

RUN pacman --noconfirm -S gcc11-fortran

RUN useradd -m user
USER user
WORKDIR /home/user

#
# Spack
#

RUN mkdir -p ~/.spack \
  && git clone --depth 1 https://github.com/w8jcik/spack.git ~/.spack/root

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack compiler find

RUN [ "bash", "-c", "echo -e 'packages:\n  all:\n    compiler: [gcc@11.3.0]' > ~/.spack/packages.yaml" ]

## Spack is unpredictable when reusing packages
RUN [ "bash", "-c", "echo -e 'concretizer:\n  reuse: false' > ~/.spack/concretizer.yaml" ]

#
# GCC 
#

# Dependencies installed separately for easier debugging

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install --only dependencies gcc@12.2.0+binutils

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gcc@9.5.0+binutils

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gcc@6.5.0+binutils

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack load gcc@9.5.0 \
  && spack compiler find

RUN [ "bash", "-c", "echo -e 'packages:\n  all:\n    compiler: [gcc@9.5.0]' > ~/.spack/packages.yaml" ]

RUN echo "source ~/.spack/root/share/spack/setup-env.sh" > ~/.bashrc

#
# Example GROMACS installation
#

RUN . ~/.spack/root/share/spack/setup-env.sh \
  && spack install gromacs@2022.5+cuda+mpi
