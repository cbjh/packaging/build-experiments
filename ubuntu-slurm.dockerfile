ARG UBUNTU_VERSION="22.04"

FROM ubuntu:${UBUNTU_VERSION}

ARG UBUNTU_VERSION

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y

RUN \
  if [ "$UBUNTU_VERSION" = "14.04" ] ; then \
    apt-get install -y slurm-llnl ; \
  else \
    apt-get install -y slurmd slurmctld ; \
  fi

# Slurm configuration in Ubuntu 22.04
ADD ubuntu-slurm/etc/slurm/slurm.conf /etc/slurm/slurm.conf

# Slurm configuration in Ubuntu 20.04, 18.04, 16.04, 14.04
ADD ubuntu-slurm/etc/slurm-llnl/slurm.conf /etc/slurm-llnl/slurm.conf

RUN ( cd /var/spool && mkdir -p slurmctld && chown slurm: /var/spool/slurmctld )
RUN ( mkdir /run/munge && chown munge: /run/munge )

RUN \
  if [ "$UBUNTU_VERSION" = "14.04" ] ; then \
    create-munge-key ; \
  fi

RUN useradd -m user
USER user
WORKDIR /home/user
SHELL ["/bin/bash", "-c"]

USER root

# On Ubuntu 16.04 hwloc throws an error when parsing /proc/mount. It should be safe to ignore it.
# https://stackoverflow.com/questions/46138549/docker-openmpi-and-unexpected-end-of-proc-mounts-line
CMD \
  if [ "$(lsb_release -cs)" = "trusty" ]; then \
    service munge start && \
    service slurm-llnl start && \
    su -s /bin/bash - user ; \
  else \
    service munge start && \
    service slurmctld start && \
    service slurmd start && \
    su -s /bin/bash - user ; \
  fi
