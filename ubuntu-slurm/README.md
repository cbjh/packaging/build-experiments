# Test Slurm

```bash
$ sinfo
$ srun --pty bash -i  # should not crash, ctrl + d to exit
$ sbatch --wrap "echo 'example' > test"
$ cat test
example
```
