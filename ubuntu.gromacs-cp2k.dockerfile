FROM ubuntu:22.04

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get upgrade -y

# CUDA 11.5.1 and GCC 11.2
RUN apt-get install -y nvidia-cuda-toolkit nvidia-cuda-dev

ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility

RUN apt-get install -y build-essential cmake git

RUN apt-get install -qq --no-install-recommends \
  autoconf autogen automake autotools-dev bzip2 ca-certificates git less \
  libtool libtool-bin make nano patch pkg-config python3 unzip wget xxd zlib1g-dev

RUN apt-get install -y gcc g++ gfortran
RUN apt-get install -y gcc-10 g++-10 gfortran-10

RUN apt-get install -qq libopenblas-dev libfftw3-dev

RUN useradd -m user
USER user
WORKDIR /home/user
SHELL ["/bin/bash", "-c"]

RUN git clone --recursive -b v2022.2 https://github.com/cp2k/cp2k.git

RUN cd "${HOME}/cp2k/tools/toolchain/" \
    && ./install_cp2k_toolchain.sh --with-libxsmm=install \
        --with-openblas=system --with-fftw=system \
        --enable-cuda --gpu-ver=V100

RUN cp "${HOME}/cp2k/tools/toolchain/install/arch/"* "${HOME}/cp2k/arch/"

RUN source "${HOME}/cp2k/tools/toolchain/install/setup" \
    && cd cp2k \
    && make -j ARCH=local VERSION=ssmp \
    && make -j ARCH=local VERSION=ssmp libcp2k

# RUN source "${HOME}/cp2k/tools/toolchain/install/setup" \
#     && cd cp2k \
#     && make -j ARCH=local VERSION=ssmp test

RUN git clone -b v2022.4 https://github.com/gromacs/gromacs.git gromacs-cp2k-src

RUN source "${HOME}/cp2k/tools/toolchain/install/setup" \
    && DIR="gromacs-cp2k" \
    && cmake \
        -DBUILD_SHARED_LIBS=OFF -DGMXAPI=OFF -DGMX_INSTALL_NBLIB_API=OFF \
        -DGMX_EXTERNAL_BLAS=ON -DGMX_EXTERNAL_LAPACK=ON \
        -DGMX_CP2K=ON -DCP2K_DIR="${HOME}/cp2k/lib/local/ssmp" \
        -DGMX_GPU=cuda \
        -DCMAKE_C_COMPILER=gcc-10 -DCMAKE_CXX_COMPILER=g++-10 -DCMAKE_FORTRAN_COMPILER=gfortran-10 \
        -DCMAKE_INSTALL_PREFIX=`pwd`/${DIR} -S "${DIR}-src" -B "${DIR}-build" \
    && cmake --build "${DIR}-build" --target install -j $(nproc)

RUN source gromacs-cp2k/bin/GMXRC.bash \
    && gmx --version
