FROM ubuntu:22.04

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get upgrade -y

# CUDA 11.5.1 and GCC 11.2
RUN apt-get install -y --no-install-recommends nvidia-cuda-toolkit nvidia-cuda-dev

RUN apt-get install -y build-essential cmake git

# FFTW 3.3.8 (you don't need it, it is just for this script to run faster)
RUN apt-get install -y libfftw3-dev

# GCC 10.3 (needed by GROMACS 2022, 2021, 2020 and 2016)
RUN apt-get install -y gcc-10 g++-10

RUN useradd -m user
USER user
WORKDIR /home/user


#
# GROMACS 2022
#

ARG DIR="gromacs-2022"

RUN git clone --depth 1 --branch v2022.2 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"

# GCC 11 fails to build GROMACS 2022, GCC 10 is selected instead
# Add -DGMX_BUILD_OWN_FFTW=ON when calling this command (we don't do it, to save time)
RUN cmake -DGMX_GPU="cuda" -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD="AVX2_256" -DCMAKE_C_COMPILER=gcc-10 -DCMAKE_CXX_COMPILER=g++-10 -S "${DIR}-src" -B "${DIR}-build"

RUN cmake --build "${DIR}-build" -j "$(nproc)" --target install


#
# GROMACS 2021
#

ARG DIR="gromacs-2021"

RUN git clone --depth 1 --branch v2021.6 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"

# GCC 11 fails to build GROMACS 2021, GCC 10 is selected instead
# Add -DGMX_BUILD_OWN_FFTW=ON when calling this command (we don't do it, to save time)
RUN cmake -DGMX_GPU="cuda" -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD="AVX2_256" -DCMAKE_C_COMPILER=gcc-10 -DCMAKE_CXX_COMPILER=g++-10 -S "${DIR}-src" -B "${DIR}-build"

RUN cmake --build "${DIR}-build" -j "$(nproc)" --target install


#
# GROMACS 2020
#

ARG DIR="gromacs-2020"

RUN git clone --depth 1 --branch v2020.7 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"

# # Some headers are missing and build fails with GCC 11, patches from https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/gromacs/package.py#L194
# RUN \
#   sed -i "s/#include <vector>/#include <vector>\n#include <limits>/" "${DIR}-src/src/gromacs/awh/biasparams.h" && \
#   sed -i "s/#include <vector>/#include <vector>\n#include <limits>/" "${DIR}-src/src/gromacs/mdrun/minimize.cpp" && \
#   sed -i "s/#include <queue>/#include <queue>\n#include <limits>/" "${DIR}-src/src/gromacs/modularsimulator/modularsimulator.h"

# Remove invalid CUDA detection script, patches from https://gitlab.com/cbjh/gromacs-swaxs/-/commit/5fe3fb46f60c73231d3af9136d95b99f09b8a7b3
RUN \
  sed -i "s|cmake/FindCUDA\\\\\\\\\\\\\\\.cmake||" "${DIR}-src/cmake/gmxCPackUtilities.cmake" && \
  sed -i "s|cmake/FindCUDA||" "${DIR}-src/cmake/gmxCPackUtilities.cmake" && \
  rm -rf "${DIR}-src/cmake/FindCUDA" && rm "${DIR}-src/cmake/FindCUDA.cmake"

# GROMACS 2020 does not detect RTX A4000
RUN sed -i 's/# Next add flags that trigger PTX code generation/if(NOT CUDA_VERSION VERSION_LESS "11.1")\n        list (APPEND GMX_CUDA_NVCC_GENCODE_FLAGS "-gencode;arch=compute_86,code=sm_86")\n    endif()\n\n    # Next add flags that trigger PTX code generation/' "${DIR}-src/cmake/gmxManageNvccConfig.cmake"

# GCC 11 fails to build GROMACS 2020, GCC 10 is selected instead
# Add -DGMX_BUILD_OWN_FFTW=ON when calling this command (we don't do it, to save time)
RUN cmake -DGMX_GPU="cuda" -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD="AVX2_256" -DCMAKE_C_COMPILER=gcc-10 -DCMAKE_CXX_COMPILER=g++-10 -S "${DIR}-src" -B "${DIR}-build"

RUN cmake --build "${DIR}-build" -j "$(nproc)" --target install


#
# GROMACS 2019
#

ARG DIR="gromacs-2019"

RUN git clone --depth 1 --branch v2019.6 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"

# Some headers are missing and build fails with GCC 11, patches from https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/gromacs/package.py#L194
RUN \
  sed -i "s/#include <vector>/#include <vector>\n#include <limits>/" "${DIR}-src/src/gromacs/awh/biasparams.h" && \
  sed -i "s/#include <vector>/#include <vector>\n#include <limits>/" "${DIR}-src/src/gromacs/mdrun/minimize.cpp"

# Remove invalid CUDA detection script, patches from https://gitlab.com/cbjh/gromacs-swaxs/-/commit/5fe3fb46f60c73231d3af9136d95b99f09b8a7b3
RUN \
  sed -i "s|cmake/FindCUDA\\\\\\\\\\\\\\\.cmake||" "${DIR}-src/cmake/gmxCPackUtilities.cmake" && \
  sed -i "s|cmake/FindCUDA||" "${DIR}-src/cmake/gmxCPackUtilities.cmake" && \
  rm -rf "${DIR}-src/cmake/FindCUDA" && rm "${DIR}-src/cmake/FindCUDA.cmake"

# CUDA 11 doesn't support NVidia 3.0, patches from https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/gromacs/package.py#L213
RUN \
  sed -i -E 's/-gencode;arch=compute_30,code=sm_30;?//g'       "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; \
  sed -i -E 's/-gencode;arch=compute_30,code=compute_30;?//g'  "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; true

# GROMACS 2019 does not detect RTX A4000
RUN sed -i 's/# Next add flags that trigger PTX code generation/if(NOT CUDA_VERSION VERSION_LESS "11.1")\n        list (APPEND GMX_CUDA_NVCC_GENCODE_FLAGS "-gencode;arch=compute_86,code=sm_86")\n    endif()\n\n    # Next add flags that trigger PTX code generation/' "${DIR}-src/cmake/gmxManageNvccConfig.cmake"

# Add -DGMX_BUILD_OWN_FFTW=ON when calling this command (we don't do it, to save time)
RUN cmake -DGMX_GPU="cuda" -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD="AVX2_256" -S "${DIR}-src" -B "${DIR}-build"

RUN cmake --build "${DIR}-build" -j "$(nproc)" --target install


#
# GROMACS 2018
#

ARG DIR="gromacs-2018"

RUN git clone --depth 1 --branch v2018.8 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"

# Some headers are missing and build fails with GCC 11, patches from https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/gromacs/package.py#L194
RUN \
  sed -i "s/#include <vector>/#include <vector>\n#include <limits>/" "${DIR}-src/src/gromacs/awh/biasparams.h" && \
  sed -i "s/#include <vector>/#include <vector>\n#include <limits>/" "${DIR}-src/src/gromacs/mdlib/minimize.cpp"

# Remove invalid CUDA detection script, patches from https://gitlab.com/cbjh/gromacs-swaxs/-/commit/5fe3fb46f60c73231d3af9136d95b99f09b8a7b3
RUN \
  sed -i "s|cmake/FindCUDA\\\\\\\\\\\\\\\.cmake||" "${DIR}-src/cmake/gmxCPackUtilities.cmake" && \
  sed -i "s|cmake/FindCUDA||" "${DIR}-src/cmake/gmxCPackUtilities.cmake" && \
  rm -rf "${DIR}-src/cmake/FindCUDA" && rm "${DIR}-src/cmake/FindCUDA.cmake"

# CUDA >= 11 doesn't support NVidia 3.0, patches from https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/gromacs/package.py#L213
RUN \
  sed -i -E 's/-gencode;arch=compute_30,code=sm_30;?//g'       "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; \
  sed -i -E 's/-gencode;arch=compute_30,code=compute_30;?//g'  "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; true

# GROMACS 2018 does not detect RTX A4000
RUN sed -i 's/# Next add flags that trigger PTX code generation/if(NOT CUDA_VERSION VERSION_LESS "11.1")\n        list (APPEND GMX_CUDA_NVCC_GENCODE_FLAGS "-gencode;arch=compute_86,code=sm_86")\n    endif()\n\n    # Next add flags that trigger PTX code generation/' "${DIR}-src/cmake/gmxManageNvccConfig.cmake"

# Add -DGMX_BUILD_OWN_FFTW=ON when calling this command (we don't do it, to save time)
RUN cmake -DGMX_GPU="cuda" -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD="AVX2_256" -S "${DIR}-src" -B "${DIR}-build"

RUN cmake --build "${DIR}-build" -j "$(nproc)" --target install


#
# GROMACS 2016
#

ARG DIR="gromacs-2016"

RUN git clone --depth 1 --branch v2016.6 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"

# CUDA >= 11 doesn't support NVidia 3.0, patches from https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/gromacs/package.py#L213
RUN \
  sed -i -E 's/-gencode;arch=compute_30,code=sm_30;?//g'       "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; \
  sed -i -E 's/-gencode;arch=compute_30,code=compute_30;?//g'  "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; true

# GCC 11 fails to build GROMACS 2016, GCC 10 is selected instead
# Add -DGMX_BUILD_OWN_FFTW=ON when calling this command (we don't do it, to save time)
RUN cmake -DGMX_GPU="cuda" -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD="AVX2_256" -DCMAKE_C_COMPILER=gcc-10 -DCMAKE_CXX_COMPILER=g++-10 -S "${DIR}-src" -B "${DIR}-build"

RUN cmake --build "${DIR}-build" -j "$(nproc)" --target install
