FROM archlinux:base

RUN pacman --noconfirm -Syu

# GCC 12.2.1 as gcc, GCC 11.3.0 as gcc-11

RUN pacman --noconfirm -S --needed base-devel
RUN pacman --noconfirm -S cmake git

# CUDA 11.8

RUN pacman --noconfirm -S cuda

RUN useradd -m user
USER user
WORKDIR /home/user

# Install GROMACS 2021

ARG DIR="gromacs-2021"
RUN git clone --depth 1 --branch release-2021 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"
RUN cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-11 -DCMAKE_CXX_COMPILER=g++-11 -S "${DIR}-src" -B "${DIR}-build"
RUN cmake --build "${DIR}-build" --target install -- -j 6

# Install GROMACS 2020

ARG DIR="gromacs-2020"
RUN git clone --depth 1 --branch release-2020 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"
RUN cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-11 -DCMAKE_CXX_COMPILER=g++-11 -S "${DIR}-src" -B "${DIR}-build"
RUN cmake --build "${DIR}-build" --target install -- -j 6

# Install GROMACS 2019

ARG DIR="gromacs-2019"
RUN git clone --depth 1 --branch release-2019 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"
RUN cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DCMAKE_C_COMPILER=gcc-11 -DCMAKE_CXX_COMPILER=g++-11 -S "${DIR}-src" -B "${DIR}-build"
RUN cmake --build "${DIR}-build" --target install -- -j 6

# Install GROMACS 2018

ARG DIR="gromacs-2018"
RUN git clone --depth 1 --branch release-2018 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"
RUN cmake -DGMX_GPU="cuda" -DGMX_BUILD_OWN_FFTW=ON -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" -DGMX_SIMD=AVX2_256 -DCMAKE_C_COMPILER=gcc-11 -DCMAKE_CXX_COMPILER=g++-11 -S "${DIR}-src" -B "${DIR}-build"
RUN cmake --build "${DIR}-build" --target install -- -j 6
