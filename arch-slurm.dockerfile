FROM archlinux:base-devel

RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -Sy python git
RUN pacman --noconfirm -Sy gtk2 hwloc rrdtool hdf5

RUN useradd -m user
USER user
WORKDIR /home/user

RUN git clone https://aur.archlinux.org/munge.git
RUN git clone https://aur.archlinux.org/slurm-llnl.git

RUN cd munge && makepkg

USER root

RUN cd munge && pacman --noconfirm -U munge-*.pkg.tar.zst

USER user

# 22.05 (builds)

# # 21.08 (builds)
# RUN cd slurm-llnl && git checkout 9a8377f5888d8eb5a89334110f55c70db4a65220

# # 20.11 (builds)
# RUN cd slurm-llnl && git checkout a49b3d54d6dc432d576f12fd710901a10ff671f5

# # 19.05 (doesn't build)
# RUN cd slurm-llnl && git checkout 0dfe634723c24b90e3579682cf1b7b103dabb156

# # 18.08 (doesn't build)
# RUN cd slurm-llnl && git checkout c077d9edc1b8eb591759193fe72f08d03c3b1f7f

# # 17.11 (doesn't link)
# RUN cd slurm-llnl && git checkout 3510ac4853fe318dc42d424a01020de8e7f830ec

# # 16.05 (doesn't download)
# RUN cd slurm-llnl && git checkout cda69b96c36eee1efad52107f69d79526ebfee98

RUN cd slurm-llnl && makepkg

USER root

RUN cd slurm-llnl && pacman --noconfirm -U slurm-llnl-*.pkg.tar.zst

USER user

# RUN mkdir -p ~/.spack \
#   && git clone --depth 1 https://github.com/spack/spack.git ~/.spack/root

# RUN . ~/.spack/root/share/spack/setup-env.sh \
#   && spack spec --only dependencies openmpi

# 17.11 no longer builds PMI library https://bugs.schedmd.com/show_bug.cgi?id=4704

# smaug $ cd lib && find -name *pmi*
# ./slurm/mpi_pmi2.so
# ./slurm/libslurm_pmi.so

# lautern $ cd lib && find -name *pmi*
# ./slurm/mpi_pmi2.so
