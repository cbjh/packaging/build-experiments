ARG UBUNTU_VERSION="22.04"

FROM ubuntu:${UBUNTU_VERSION}

ARG UBUNTU_VERSION

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get upgrade -y

RUN apt-get install -y build-essential git cmake

# GCC 11 doesn't build GROMACS 2021/2022 with CUDA enabled
RUN \
  if [ "$UBUNTU_VERSION" = "22.04" ] ; then \
    apt-get install -y gcc-10 g++-10 ; \
  fi

# GCC >= 7 for older Ubuntu
RUN \
  if [ "$UBUNTU_VERSION" = "16.04" ] ; then \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:jonathonf/gcc && \
    apt-get update -y && \
    apt-get install -y gcc-7 g++-7 ; \
  fi

RUN \
  if [ "$UBUNTU_VERSION" = "14.04" ] ; then \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:bryce-schober/trusty-gcc-backport-copies-ppa && \
    apt-get update -y && \
    apt-get install -y gcc-7 g++-7 ; \
  fi

# Installing inefficient version of FFTW library, to build GROMACS faster.
# For normal usage, let GROMACS build own FFTW.
RUN apt-get install -y libfftw3-dev

# Ubuntu 22.04 gets CUDA 11.5, Ubuntu 20.04 gets CUDA 10.1
RUN \
  if [ "$UBUNTU_VERSION" = "22.04" ] || [ "$UBUNTU_VERSION" = "20.04" ] ; then \
    apt-get install -y --no-install-recommends nvidia-cuda-toolkit nvidia-cuda-dev ; \
  fi

# CUDA 10.2 (11.7 available)
RUN \
  if [ "$UBUNTU_VERSION" = "18.04" ] ; then \
    apt-get install -y wget && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/3bf863cc.pub && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub && \
    wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.2.89-1_amd64.deb && \
    dpkg -i cuda-repo-ubuntu1804_10.2.89-1_amd64.deb && \
    apt-get update -y ; \
    apt-get install -y --no-install-recommends cuda-toolkit-10-2 ; \
  fi

# CUDA 10.2 (11.3 available)
RUN \
  if [ "$UBUNTU_VERSION" = "16.04" ] ; then \
    apt-get install -y wget && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/3bf863cc.pub && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub && \
    wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_10.2.89-1_amd64.deb && \
    dpkg -i cuda-repo-ubuntu1604_10.2.89-1_amd64.deb && \
    apt-get update -y ; \
    apt-get install -y --no-install-recommends cuda-toolkit-10-2 ; \
  fi

# CUDA 10.1
RUN \
  if [ "$UBUNTU_VERSION" = "14.04" ] ; then \
    apt-get install -y wget && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub && \
    wget http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1404/x86_64/cuda-repo-ubuntu1404_10.1.243-1_amd64.deb && \
    dpkg -i cuda-repo-ubuntu1404_10.1.243-1_amd64.deb && \
    apt-get update -y ; \
    apt-get install -y --no-install-recommends cuda-toolkit-10-1 ; \
  fi

# Provides `nvidia-smi` command. Calling it is a reliable way to test if GPU is visible. Otherwise, you can skip this step.
RUN \
  if [ "$UBUNTU_VERSION" = "22.04" ] || [ "$UBUNTU_VERSION" = "20.04" ] ; then \
    apt-get install -y nvidia-utils-510 ; \
  fi

# Used for patching of GROMACS CMake files
RUN apt-get install -y sed

RUN useradd -m user
USER user
WORKDIR /home/user

# At this point, one can test with `nvidia-smi` command. After successfull setup, it should show state of GPU. Otherwise it crashes.
#
# In case it crashes, first check your host
# - Make sure that `nvidia-docker2` is installed.
# - Make sure that propertiary `nvidia-driver` is enabled in _Additional Drivers_ (for example `nvidia-driver-510`, do not select open source _Nouveau_).
# - Make sure that `docker run` has `--gpus all` switch present.


# Checking if GROMACS builds against installed CUDA.

# GROMACS 2022.1 requires CUDA 11.0
# GROMACS 2021.5 requires CUDA 9.0
# GROMACS 2020.7 requires CUDA 9.0
# GROMACS 2019.6 requires CUDA 7.0
# GROMACS 2018.8 requires CUDA 6.5
# GROMACS 2016.6 requires CUDA 5.0

# Ubuntu 22.04 provides CUDA 11.5
# Ubuntu 20.04 provides CUDA 10.1
# Ubuntu 18.04 provides CUDA 10.2 (11.7 available, upstream 9.1)
# Ubuntu 16.04 provides CUDA 10.2 (11.3 available, upstream 7.5)
# Ubuntu 14.04 provides CUDA 10.1 (upstream 5.5)

RUN \
  if [ "$UBUNTU_VERSION" = "18.04" ] || [ "$UBUNTU_VERSION" = "16.04" ] || [ "$UBUNTU_VERSION" = "14.04" ] ; then \
    ( \
      mkdir cmake && \
      cd cmake && \
      wget https://github.com/Kitware/CMake/releases/download/v3.13.5/cmake-3.13.5-Linux-x86_64.sh && \
      chmod +x cmake-3.13.5-Linux-x86_64.sh && \
      ./cmake-3.13.5-Linux-x86_64.sh --skip-license && \
      rm cmake-3.13.5-Linux-x86_64.sh ; \
    ) \
  fi

ENV PATH="/home/user/cmake/bin:${PATH}"

ARG DIR="gromacs"
ARG GROMACS_REF="v2021.5"

RUN git config --global advice.detachedHead false
RUN git clone --branch "${GROMACS_REF}" --depth 1 https://gitlab.com/gromacs/gromacs.git "${DIR}-src"

# Patches from https://github.com/spack/spack/blob/develop/var/spack/repos/builtin/packages/gromacs/package.py#L213
RUN \
  sed -i -E 's/-gencode;arch=compute_30,code=sm_30;?//g'       "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; \
  sed -i -E 's/-gencode;arch=compute_30,code=compute_30;?//g'  "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; \
  sed -i -E 's/-gencode;arch=compute_20,code=sm_20;?//g'       "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; \
  sed -i -E 's/-gencode;arch=compute_20,code=compute_20;?//g'  "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; \
  sed -i -E 's/-gencode;arch=compute_20,code=sm_21;?//g'       "${DIR}-src/cmake/gmxManageNvccConfig.cmake" ; true

RUN \
  cmake -DGMX_GPU=cuda -DCMAKE_INSTALL_PREFIX="$(pwd)/${DIR}" \
    -DCMAKE_C_COMPILER="$(which gcc-8 || which gcc-7 || which gcc-10 || which gcc-9 || which gcc)" \
    -DCMAKE_CXX_COMPILER="$(which g++-8 || which g++-7 || which g++-10 || which g++-9 || which g++)" \
    -S "${DIR}-src" -B "${DIR}-build" && \
  cmake --build "${DIR}-build" --target install -j $(nproc)

ENV PATH="/home/user/gromacs/bin:${PATH}"

USER root

# OpenCL for Yasara, normally installed by `libnvidia-compute-[1-9]+`
RUN mkdir -p /etc/OpenCL/vendors && \
    echo "libnvidia-opencl.so.1" > /etc/OpenCL/vendors/nvidia.icd

# Calling `clinfo` is a simple test of OpenCL being available
RUN apt-get install -y clinfo

USER user
