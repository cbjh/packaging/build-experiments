FROM archlinux

RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -Sy base-devel
RUN pacman --noconfirm -Sy git wget
RUN pacman --noconfirm -Sy python python-pip python-setuptools
RUN pacman --noconfirm -Sy sudo
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers

RUN useradd -m user
USER user
WORKDIR /home/user

# GCC 12

USER root

RUN pacman --noconfirm -S gcc-fortran

USER user

# GCC 11

USER root

RUN pacman --noconfirm -S gcc11 gcc11-fortran

USER user

# AUR

USER root

RUN echo 'MAKEFLAGS="-j8"' >> /etc/makepkg.conf

USER user

# GCC 9 from AUR

RUN wget https://aur.archlinux.org/cgit/aur.git/snapshot/gcc9.tar.gz && tar -xzf gcc9.tar.gz \
  && ( cd gcc9 && makepkg --skippgpcheck --syncdeps --noconfirm --install ) \
  && rm -rf gcc9 && rm gcc9.tar.gz

# GCC 6 from AUR

RUN wget https://aur.archlinux.org/cgit/aur.git/snapshot/libart-lgpl.tar.gz && tar -xzf libart-lgpl.tar.gz \
  && ( cd libart-lgpl && makepkg --noconfirm --install ) \
  && rm -rf libart-lgpl && rm libart-lgpl.tar.gz

# I am not sure what the workaround below is doing, but it is the same as in GCC 9 package.
RUN wget https://aur.archlinux.org/cgit/aur.git/snapshot/gcc6.tar.gz && tar -xzf gcc6.tar.gz \
  && sed -i "117 i   LD_PRELOAD='/usr/lib/libstdc++.so' \\\\" gcc6/PKGBUILD \
  && ( cd gcc6 && makepkg --syncdeps --noconfirm --install ) \
  && rm -rf gcc6 && rm gcc6.tar.gz
