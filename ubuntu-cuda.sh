#!/usr/bin/bash

set -euo pipefail

SCRIPT_DIR=$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)

for ubuntu_version in "14" "16" "18" "20" "22" ; do
    for gromacs_version in "5.1.5" "2016.6" "2018.8" "2019.6" "2020.7" "2021.5" ; do
        gromacs_ref_slug="${gromacs_version//./-}"
        tag="temp-ubuntu-${ubuntu_version}-cuda-gromacs-${gromacs_ref_slug}"

        if [ "${ubuntu_version}" = "20" ] ; then
            if [ "${gromacs_version}" = "2018.8" ]; then
                continue
            fi

            if [ "${gromacs_version}" = "2019.6" ]; then
                continue
            fi

            if [ "${gromacs_version}" = "2020.7" ]; then
                continue
            fi
        fi

        if [ "${ubuntu_version}" = "22" ] ; then
            if [ "${gromacs_version}" = "2016.6" ]; then
                continue
            fi

            if [ "${gromacs_version}" = "2018.8" ]; then
                continue
            fi

            if [ "${gromacs_version}" = "2019.6" ]; then
                continue
            fi

            if [ "${gromacs_version}" = "2020.7" ]; then
                continue
            fi
        fi

        if [[ "$(docker images -q "${tag}" 2> /dev/null)" == "" ]]; then
            echo "Building ${tag}"

            docker build --build-arg UBUNTU_VERSION="${ubuntu_version}.04" --build-arg GROMACS_REF="v${gromacs_version}" -t "${tag}" -f "${SCRIPT_DIR}/ubuntu-cuda.dockerfile" . && \
                docker run --pid=host --gpus all -t "${tag}" nvidia-smi && \
                docker run --pid=host --gpus all -t "${tag}" clinfo && \
                docker run --pid=host --gpus all -t "${tag}" gmx --version || { echo "Failed to build ${tag}" ; exit 1 ; }
        else
            echo "Skipping ${tag}"
        fi
    done
done
